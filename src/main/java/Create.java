import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
public class Create {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Car car = new Car();
        car.setName("Volga");
        car.setPrice(12000);
        session.saveOrUpdate(car);
        transaction.commit();
        session.close();
        sessionFactory.close();
    }
}
